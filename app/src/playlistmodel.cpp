/*
 * Copyright (C) 2019 - Stefan Weng <stefwe@mailbox.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "playlistmodel.h"
//-------------------------------------------------------------------------------------------------

PlayListModel::PlayListModel(QObject *parent) : QAbstractListModel(parent)
{

}
//-------------------------------------------------------------------------------------------------
int PlayListModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return m_textItems.size();

}
//-------------------------------------------------------------------------------------------------
QVariant PlayListModel::data(const QModelIndex &index, int role) const
{
    if(m_Pos.size() != 0)
    {
        if (role == Qt::UserRole)
        {
            return QVariant(m_Pos[index.row()]);
        }
        if (role == Qt::UserRole+1)
        {
            return QVariant(m_textItems[index.row()]);
        }
        if (role == Qt::UserRole+2)
        {
            return QVariant(m_otherTextItems[index.row()]);
        }
        if (role == Qt::UserRole+3)
        {
            return QVariant(m_otherOtherItems[index.row()]);
        }
        
        return QVariant();
    }
    return QVariant();

}

QHash<int,QByteArray> PlayListModel::roleNames() const {
  
  QHash<int, QByteArray> roles;
  roles[Qt::UserRole]="pos";
  roles[Qt::UserRole+1]="uid";
  roles[Qt::UserRole+2]="title";
  roles[Qt::UserRole+3]="uri";
  
  return roles;
}

//-------------------------------------------------------------------------------------------------
void PlayListModel::appendRow(int pos, QString textData, QString otherTextData, QString anotherTextData)
{
    int lastElemPos = m_Pos.size();
    beginInsertRows(QModelIndex(), lastElemPos, lastElemPos);
    m_Pos << pos;
    m_textItems << textData;
    m_otherTextItems << otherTextData;
    m_otherOtherItems << anotherTextData;
    
    endInsertRows();
}
//-------------------------------------------------------------------------------------------------
void PlayListModel::myRemoveRows(int position, int rows)
{
    beginRemoveRows(QModelIndex(), position, position+rows-1);
    for (int row=0; row < rows; ++row) 
    {
        m_Pos.removeAt(position);
        m_textItems.removeAt(position);
        m_otherTextItems.removeAt(position);
        m_otherOtherItems.removeAt(position);
    }

    endRemoveRows();    
}
//-------------------------------------------------------------------------------------------------
void PlayListModel::removeLastItem()
{
    if(m_Pos.size() > 0)
    {
        int lastElemPos = m_Pos.size() - 1;    
        myRemoveRows(lastElemPos, 1);
    }
}

void PlayListModel::removeFromItems(int position)
{
  if(m_Pos.size() > 0)
  {
    int lastElemPos = m_Pos.size();
    for (int i=lastElemPos; i > position; i--) 
    {
      myRemoveRows(i, 1);
    }
  }
}

void PlayListModel::removeAllItems()
{
  while(m_Pos.size() > 0)
  {
    int lastElemPos = m_Pos.size() - 1;    
    myRemoveRows(lastElemPos, 1);
  }
}

void PlayListModel::loadQueue(QStringList uriList, QStringList titleList) {
  
  removeAllItems();
  
  qDebug() << "Load actual queue";
  unsigned i=0;

  for(QString item: uriList) {  //playList.count() = gibt die Länge von QStringList zurück
    appendRow(0, QString::number(i), titleList.value(i), item);
    i++;
  }
  
}

void PlayListModel::loadPlaylists(QStringList playlists) {
  
  removeAllItems();
  
  qDebug() << "Load Playlists";
  unsigned i=0;
  
  for(QString item: playlists) {  //playList.count() = gibt die Länge von QStringList zurück
    appendRow(0, QString::number(i), item, "");
    i++;
  }
}

void PlayListModel::loadDirlists(QStringList entryTyp, QStringList dirTitle, QStringList dirlist) {
  
  removeAllItems();
  
  qDebug() << "Load Directory list";
  unsigned i=0;
  
  for(QString item: dirlist) {
    appendRow(i, entryTyp.value(i), dirTitle.value(i), item);
    i++;
  }
}

void PlayListModel::loadNavList(QString folderName, QString navPath) {

  qDebug() << "Add navigation item";
  
  if (navPath == "") {
    removeAllItems();
    appendRow(0, "0", "Home", "");
  } else {
    appendRow(0, QString::number(m_textItems.size()), folderName, navPath);
  }
}

void PlayListModel::removeNavList(int position, QString navPath)
{
  qDebug() << "Remove navigation item";
  
  if (navPath == "") {
    removeAllItems();
    appendRow(0, "0", "Home", "");
  } else {
    removeFromItems(position);
  }
}

void PlayListModel::loadSearchList(QStringList entryTyp, QStringList name)
{
  removeAllItems();
  
  qDebug() << "Load search list";
  unsigned i=0;
  
  for(QString item: name) {
    appendRow(0, entryTyp.value(i), item, "");
    i++;
  }
}
