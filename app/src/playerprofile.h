/*
 * Copyright (C) 2019 - Stefan Weng <stefwe@mailbox.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PLAYERPROFILE_H
#define PLAYERPROFILE_H

#include <QObject>
#include <QDebug>
#include <QDir>
#include <QStandardPaths>
#include <QFile>
#include <QSqlTableModel>
#include <QtSql>
#include <libintl.h>
#define _(value) gettext(value)

#include "mpd/client.h"
#include "profilemodel.h"
#include "utils.h"

class QSqlDatabase;
class QSqlQuery;

class PlayerProfileSettings
{
public:
    QString name;
    QString ip;
    unsigned int port;
    unsigned int timeout_ms;
    QString password;
};

class PlayerProfile : public QObject
{
  Q_OBJECT
  Q_PROPERTY(ProfileModel *profilemodel READ profilemodel WRITE setProfilemodel NOTIFY profilemodelChanged)
  Q_PROPERTY(bool noprofile READ noprofile NOTIFY noprofileChanged)
  Q_PROPERTY(QString strProfile READ strProfile NOTIFY strProfileChanged)
  Q_PROPERTY(QString strError READ strError NOTIFY strErrorChanged)
  Q_PROPERTY(bool isConnected READ isConnected NOTIFY statusConnectionChanged)
  
public:
  PlayerProfile(QObject *parent = 0);
  ~PlayerProfile();

  ProfileModel *profilemodel();
  void setProfilemodel(ProfileModel *profilemodel);
  
  bool noprofile();
  bool isConnected() const;
  
  QString strProfile() const;
  QString strError() const;
  void setStrProfile(QString profile);
  
  Q_INVOKABLE bool connectProfile(int id);
  Q_INVOKABLE void freeConnection(void);
  Q_INVOKABLE bool addProfile(QString name, QString ip, int port, int timeout, QString pw, bool connect);
  Q_INVOKABLE bool editProfile(int id, QString name, QString ip, int port, int timeout, QString pw, bool connect);
  Q_INVOKABLE void deleteProfile(int id);
  
signals:
  void profilemodelChanged(ProfileModel *profilemodel);
  void statusConnectionChanged();
  void strErrorChanged(QString strError);
  void noprofileChanged(bool noprofile);
  void strProfileChanged();
  void connectionEstablished(struct mpd_connection *connection);
  void connectionClosed();
  
private:
  void createDataDir(void);
  void createDB(QString path);
  void createTableProfile(void);
  void readAllEntrys(void);
  void closeDB(void);
  bool establishConnection(PlayerProfileSettings profile);

  struct mpd_connection *connection;
  QSqlDatabase *m_db;
  ProfileModel *m_profilemodel = nullptr;
  bool m_noprofile = true;
  bool m_connected = false;
  QString m_strProfile = "";
  QString m_strError = "";
};

#endif
