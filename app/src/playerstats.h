/*
 * Copyright (C) 2019 - Stefan Weng <stefwe@mailbox.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PLAYERSTATS_H
#define PLAYERSTATS_H

#include <QDateTime>

#include "connect.h"

class PlayerStats : public Connect
{
  Q_OBJECT
  
  Q_PROPERTY(QString updateTime READ updateTime WRITE setUpdateTime NOTIFY updateTimeChanged)
  Q_PROPERTY(unsigned int numberOfArtists READ numberOfArtists WRITE setNumberOfArtists NOTIFY updateNumberOfArtists)
  Q_PROPERTY(unsigned int numberOfAlbums READ numberOfAlbums WRITE setNumberOfAlbums NOTIFY updateNumberOfAlbums)
  Q_PROPERTY(unsigned int numberOfSongs READ numberOfSongs WRITE setNumberOfSongs NOTIFY updateNumberOfSongs)
  Q_PROPERTY(QString playTime READ playTime WRITE setPlayTime NOTIFY updatePlayTime)
  
public:
  PlayerStats(QObject *parent = 0);
  
  Q_INVOKABLE void update(void);
  
  void setUpdateTime(QString updateTime);
  QString updateTime() const;
  void checkUpdateTime(struct mpd_stats *stats);
  
  void setNumberOfArtists(unsigned int numArtists);
  unsigned int numberOfArtists() const;
  void checkNumberOfArtists(struct mpd_stats *stats);
  
  void setNumberOfAlbums(unsigned int numAlbums);
  unsigned int numberOfAlbums() const;
  void checkNumberOfAlbums(struct mpd_stats *stats);
  
  void setNumberOfSongs(unsigned int numSongs);
  unsigned int numberOfSongs() const;
  void checkNumberOfSongs(struct mpd_stats *stats);
  
  void setPlayTime(QString playTime);
  QString playTime() const;
  void checkPlayTime(struct mpd_stats *stats);
  
signals:
  void updateTimeChanged(QString updateTime);
  void updateNumberOfArtists(unsigned int numArtists);
  void updateNumberOfAlbums(unsigned int numAlbums);
  void updateNumberOfSongs(unsigned int numSongs);
  void updatePlayTime(QString playTime);
  
private:
  QString m_updateTime;
  unsigned int m_numArtists;
  unsigned int m_numAlbums;
  unsigned int m_numSongs;
  QString m_playTime;
};

#endif
