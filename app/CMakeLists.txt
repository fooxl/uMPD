set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")

execute_process(
    COMMAND dpkg-architecture -qDEB_HOST_MULTIARCH
    OUTPUT_VARIABLE ARCH_TRIPLET
    OUTPUT_STRIP_TRAILING_WHITESPACE
)

set(
    SRCS
    main.cpp
    src/umpd.cpp 
    src/plugin.cpp
    src/playlist.cpp
    src/playlistmodel.cpp
    src/playerstate.cpp
    src/playercontrol.cpp
    src/playerqueue.cpp
    src/playerdatabase.cpp
    src/playerstats.cpp
    src/playersearch.cpp
    src/playerprofile.cpp
    src/profilemodel.cpp
    src/utils.cpp
    src/connect.cpp
)
qt5_add_resources(uMPD_SRCS qml/qml.qrc)
add_executable(${PROJECT_NAME} ${SRCS} ${uMPD_SRCS})
qt5_use_modules(${PROJECT_NAME} Gui Qml Quick QuickControls2 Sql)
target_link_libraries(uMPD Qt5::Qml Qt5::Gui Qt5::Sql mpdclient)
