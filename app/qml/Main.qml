/*
 * Copyright (C) 2019 - Stefan Weng <stefwe@mailbox.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.9
import QtQuick.Layouts 1.1
import Ubuntu.Components 1.3
import QtQuick.Controls.Suru 2.2
import Umpd 1.0
import "controller"

MainView {
    id: root
    objectName: 'mainView'
    applicationName: 'umpd.stefanweng'
    automaticOrientation: true

    width: units.gu(45)
    height: units.gu(75)

    Suru.theme: theme.name === "Ubuntu.Components.Themes.SuruDark" ? Suru.Dark : Suru.Light

    Umpd{
        id: mpd
    }

    PageStack {
        id: pageStack

        anchors {
            left: parent.left
            right: parent.right
            top: parent.top
            bottom: parent.top
        }

        SettingsController { id: settings }

        Component.onCompleted: {
            connect(true)
        }
    }

    function connect(startup) {
        if ( mpd.profile.connectProfile(settings.profileIdxConnection) ) {
            if (startup) pageStack.push( Qt.resolvedUrl("qrc:/pages/MainPage.qml"))
        }
        else {
            pageStack.push( Qt.resolvedUrl("./pages/ConnectionPage.qml"))
        }
    }

    Connections {
        target: Qt.application
        onStateChanged: {
            if (Qt.application.state === Qt.ApplicationActive) {
                if (!mpd.profile.isConnected) connect(false)
            } else {
                if (mpd.profile.isConnected) mpd.profile.freeConnection()
            }
        }
    }
}
