/*
 * Copyright (C) 2019 - Stefan Weng <stefwe@mailbox.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.9
import QtQuick.Layouts 1.1
import Ubuntu.Components 1.3
import Ubuntu.Components.Popups 1.3
import QtQuick.Controls.Suru 2.2
import "../components"

Page {
    id: mainPage

    property var queue: 0
    
    header: PageHeader {
        id: header
        title: "uMPD"
        
        leadingActionBar {
            id: menu
            actions : [
                Action {
                    id: databaseIcon
                    objectName: "editIcon"
                    text: i18n.tr("Database")
                    iconName: "drive-harddisk-symbolic"
                    onTriggered: push( Qt.resolvedUrl("./DatabasePage.qml") )
                },
                Action {
                    id: settingsIcon
                    objectName: "settingsIcon"
                    text: i18n.tr("Connections")
                    iconName: "settings"
                    onTriggered: push( Qt.resolvedUrl("./ConnectionPage.qml") )
                }
            ]
            numberOfSlots: 1
        }

        trailingActionBar.actions: [
            Action {
                id: infoIcon
                objectName: "infoIcon"
                text: i18n.tr("About")
                iconName: "info"
                onTriggered: push( Qt.resolvedUrl("./AboutPage.qml") )
            },
            Action {
                id: clearIcon
                objectName: "clearIcon"
                text: i18n.tr("Clear Queue")
                iconName: "edit-delete"
                onTriggered: PopupUtils.open(clearQueue)
            },
            Action {
                id: saveQeue
                objectName: "saveAsIcon"
                text: i18n.tr("Save Queue")
                iconName: "save-as"
                onTriggered: PopupUtils.open(saveQueue)
            }
        ]

        StyleHints {
            foregroundColor: "#FFF"
            backgroundColor: UbuntuColors.orange
            dividerColor: "#85D8CE"
        }
        
    }

    WaitingBar {
        id: waitingBar
        anchors.top: header.bottom
        connectionState: mpd.profile.isConnected
        z: 10
    }

    Component {
        id: clearQueue
	
        Dialog {
            id: clearQueueDialog
            title: i18n.tr("Queue")
           
            Label { 
                text: i18n.tr("Are you sure you want to clear the queue?")
                horizontalAlignment: Text.AlignHCenter
                wrapMode: Text.WordWrap
            }
           
            Button {
                 text: i18n.tr("clear")
                 color: UbuntuColors.red
                 onClicked:
                 {
                   mpd.queue.clear()
                   PopupUtils.close(clearQueueDialog)
                 }
            }
            
            Button {
                 text: i18n.tr("cancel")
                 color: UbuntuColors.ash
                 onClicked: PopupUtils.close(clearQueueDialog)
            }
        }
    }

    Component {
        id: saveQueue
	
        Dialog {
            id: saveQueueDialog
            title: i18n.tr("Save queue as playlist")
           
            Label { 
                text: i18n.tr("Please enter the name under you want to save the queue")
                horizontalAlignment: Text.AlignHCenter
                wrapMode: Text.WordWrap
            }
           
            TextField {
                id: fieldQueueName
                horizontalAlignment: Text.AlignHCenter
                placeholderText: i18n.tr("e.g. favourite songs")
            }
           
            Button {
                text: i18n.tr("save")
                color: UbuntuColors.green
                onClicked:
                {
                  mpd.playlist.saveAs(fieldQueueName.displayText)
                  PopupUtils.close(saveQueueDialog)
                }
            }
            
            Button {
                text: i18n.tr("cancel")
                color: UbuntuColors.ash
                onClicked: PopupUtils.close(saveQueueDialog)
            }
        }
    }

    ListView {
        id: playListView
        width: mainPage.width
        height: mainPage.height - header.height -  itemButton.height - iconVolumeLow.height - units.gu(7)
        anchors.top: header.bottom
        currentIndex: mpd.status.song
        
        model: mpd.status.listmodel
        
        delegate: ListItem {

            height: modelLayout.height
        
            ListItemLayout {
                id: modelLayout
                Column {
                    id: column
                    Text {
                        width: parent.width
                        color: Suru.foregroundColor
                        wrapMode: Text.WordWrap
                        text: model.title
                    }
                    Text {
                        color: Suru.foregroundColor
                        text: model.index + 1 + ": " + model.uri
                    }
                }
            }

            MouseArea {
                anchors.fill: parent
                onClicked: mpd.control.playPos(model.index)
            }

            leadingActions: ListItemActions {
                actions: [
                    Action {
                        iconName: "delete"
                        text: i18n.tr("delete")
                        onTriggered: mpd.queue.deletePos(model.index)
                    }
                ]
            }
        }
        highlight: Rectangle { color: Suru.neutralColor; radius: 5 }
        highlightMoveDuration: 10
        highlightMoveVelocity: -1
        focus: true
    }
    
    Rectangle {
        anchors.top: progressLayout.top
        width: parent.width
        height: units.gu(20)
        color: Suru.backgroundColor
    }

    RowLayout {
        id: progressLayout
        spacing: units.gu(1)
        
        anchors.top: playListView.bottom
        anchors.horizontalCenter: parent.horizontalCenter
        
        property var widthSlider: parent.width - units.gu(5*2+3)
    
        Label {
            Layout.preferredWidth: units.gu(5)
            horizontalAlignment: Text.AlignLeft
            text: Math.floor(mpd.status.elapsedTime / 60) + ":" + String(Math.round(mpd.status.elapsedTime % 60)).padStart(2,"0");
        }
        
        Slider {
            id: progressSlider
            Layout.preferredWidth: progressLayout.widthSlider
            function formatValue(v) {
                return Math.floor(v / 60) + ":" + String(Math.round(v % 60)).padStart(2, "0");
            }
            minimumValue: 0.0
            maximumValue: mpd.status.totalTime
            value: mpd.status.elapsedTime // load value at startup
            live: false
                
            onPressedChanged: if(!pressed)  mpd.control.playSeekPos(mpd.status.song, value)
        }
        
        Label {
            Layout.preferredWidth: units.gu(5)
            horizontalAlignment: Text.AlignLeft
            text: Math.floor(mpd.status.totalTime / 60) + ":" + String(Math.round(mpd.status.totalTime % 60)).padStart(2, "0");
        }
    }
    
    Binding {
        target: progressSlider
        property: "value"
        value: mpd.status.elapsedTime
        when: !progressSlider.pressed
    }

    Item {
      id: itemButton
      anchors.top: progressLayout.bottom
      
      width: parent.width
      height: units.gu(5)
      
      property var widthButton: parent.width / 8

      Row { // The "Row" type lays out its child items in a horizontal line
        spacing: (parent.width - (itemButton.widthButton * 6)) / 7 // Places space between items
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter
        
        DIYButton {
            id: buttonBack
            
            color: UbuntuColors.orange

            width: itemButton.widthButton
            height: units.gu(5)

            iconname: "media-skip-backward"
            iconcolor: "white"
            
            onClicked: mpd.control.playPrevious()
        }

        DIYButton {
          id: buttonPlayPause
          
          color: UbuntuColors.orange

          width: itemButton.widthButton
          height: units.gu(5)

          iconname: mpd.status.playing === 2 ? "media-playback-pause" : "media-playback-start"
          iconcolor: "white"
          
          onClicked: mpd.control.play(mpd.status.playing)
        }

        DIYButton {
          id: buttonStop
          
          color: UbuntuColors.orange

          width: itemButton.widthButton
          height: units.gu(5)

          iconname: "media-playback-stop"
          iconcolor: "white"
          
          onClicked: mpd.control.stop()
        }

        DIYButton {
          id: buttonForward
          
          color: UbuntuColors.orange
          
          width: itemButton.widthButton
          height: units.gu(5)

          iconname: "media-skip-forward"
          iconcolor: "white"
          
          onClicked: mpd.control.playNext()
        }

        DIYButton {
          id: buttonRepeat

          color: mpd.status.repeat ? UbuntuColors.green : UbuntuColors.orange

          width: itemButton.widthButton
          height: units.gu(5)
          
          iconname: mpd.status.single ? "media-playlist-repeat-one" : "media-playlist-repeat"
          iconcolor: "white"

          onClicked: mpd.control.repeat(mpd.status.repeat, mpd.status.single)
        }

        DIYButton {
          id: buttonShuffle

          color: mpd.status.shuffle ? UbuntuColors.green : UbuntuColors.orange

          width: itemButton.widthButton
          height: units.gu(5)
          
          iconname: "media-playlist-shuffle"
          iconcolor: "white"

          onClicked: mpd.control.shuffle(mpd.status.shuffle)
        }
      }
    }
    
    RowLayout {
        id: volumeLayout
        spacing: units.gu(1)
        
        anchors.top: itemButton.bottom
    
        property var widthSlider: parent.width - labelActVolume.width - iconVolumeLow.width - iconVolumeHigh.width - units.gu(5)
        
        Label {
            id: labelActVolume
            Layout.preferredWidth: units.gu(3)
            Layout.preferredHeight: units.gu(3)
            
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            text: mpd.status.volume
        }
    
        Icon {
            id: iconVolumeLow
            Layout.preferredWidth: units.gu(3)
            Layout.preferredHeight: units.gu(3)
            color: Suru.foregroundColor
            name: "audio-volume-low"
        }
    
        Slider {
            id: volumeSlider
            Layout.preferredWidth: volumeLayout.widthSlider
            minimumValue: 0
            maximumValue: 100
            value: mpd.status.volume
            live: false

            onPressedChanged: if(!pressed) mpd.control.setVolume(value)
        }

        Icon {
            id: iconVolumeHigh
            Layout.preferredWidth: units.gu(3)
            Layout.preferredHeight: units.gu(3)
            color: Suru.foregroundColor
            name: "audio-volume-high"
        }
    }
    
    Binding {
        target: volumeSlider
        property: "value"
        value: mpd.status.volume
        when: !volumeSlider.pressed
    }
}
